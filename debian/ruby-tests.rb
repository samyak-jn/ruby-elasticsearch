ENV["NOTURN"] = "1"
modules = ['elasticsearch', 'elasticsearch-transport', 'elasticsearch-api',
           'elasticsearch-extensions','elasticsearch-watcher','elasticsearch-dsl']
$: << "."
modules.each { |m| $: << "#{m}/lib" }
modules.each do |m|
  $: << "#{m}/test"
  Dir["#{m}/test/unit/*.rb"].each { |f| require f }
  $:.pop
end
